function creaLista(lista)
{
    var noticias = document.getElementById("noticias");
    
    noticias.innerHTML = "";
    
    for (var i = 0; i < lista.length; i++)
    {
        var entrada = document.createElement("li");
        
        var enlace = document.createElement("a");
        enlace.href = lista[i].web_url;
        enlace.title = lista[i].snippet;
        enlace.appendChild(document.createTextNode(lista[i].snippet));
        
        var fecha = document.createTextNode(new Date(lista[i].pub_date).toLocaleString() + " - ");
        
        entrada.appendChild(fecha);
        entrada.appendChild(enlace);
        
        noticias.appendChild(entrada);
    }
    
    if (document.getElementById("logo") == null)
    {
        var logo = document.createElement("a");
        logo.href = "http://developer.nytimes.com/";
        logo.title = "Data provided by The New York Times";
        logo.id = "logo";
        
        var img = document.createElement("img");
        img.src = "http://static01.nytimes.com/packages/images/developer/logos/poweredby_nytimes_150a.png";
        
        logo.appendChild(img);
        
        noticias.parentElement.appendChild(logo);
    }
}

function procesaRespuesta()
{
    if (this.peticion.readyState == miAjax.READY_STATE_COMPLETE && this.peticion.status == 200)
    {
        var respuesta_json = this.peticion.responseText;
        var respuesta = JSON.parse(respuesta_json);
        
        creaLista(respuesta.response.docs);
    }
}

function procesaCarga()
{
    document.getElementById("noticias").innerHTML = "Cargando...";
}

function obtenerDisponibilidad(fechaMin, fechaMax)
{    
    var busqueda = document.getElementById("busqueda").value;
    var orden = document.getElementById("orden").value;

    var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
    url += "?api-key=9614f98f73dd4821a38f4ad57ee1f5de";
    url += '&fq=body:("' + busqueda + '") OR headline:("' + busqueda + '")';
    url += "&begin_date=" + fechaMin;
    url += "&end_date=" + fechaMax;
    url += orden == "modernas" ? "&sort=newest" : "&sort=oldest";
    var procesaError = null;
    var metodo = "GET"; // Requiere GET, no POST
    var parametros = null;
    var contentType = "application/x-www-form-urlencoded";
    var procesaCaducidad = null;
    var caducidad = null;
    var cargador = new miAjax.CargadorContenidos(
                url,
                procesaRespuesta,
                procesaError,
                procesaCarga,
                metodo,
                parametros,
                contentType,
                procesaCaducidad,
                caducidad);
}

function comprobarCampos()
{
    var fechaMin = document.getElementById("fechaMin").value;
    var fechaMax = document.getElementById("fechaMax").value;
    
    // No es posible realizar esta comprobación desde HTML
    fechaMin > fechaMax ? alert("La fecha mínima no puede ser inferior a la fecha máxima.") : obtenerDisponibilidad(fechaMin, fechaMax);
    
    return false; // Evita que se actualice la página
}
